#include <mbed.h>

#define WDT_TIMEOUT 100
#define PROCESSING_PERIOD 1

#define SCL B6
#define SDA B7
#define I2C_BITRATE 25000

#define CAN_TX PA_12
#define CAN_RX PA_11
#define CAN_BITRATE 1000000

AnalogIn AN0(A0),AN1(A1),AN2(A2),AN3(A3),AN4(A4),AN5(B0);
DigitalOut GreenLED(B8,true),RedLED(B9,false),RTS(A8,false),ExtSPIEn(B1,false);
BusOut SS(C13,C14,C15);
Serial TTY(B10,B11,1000000),RS485(A9,A10,115200);
I2C i2c(SDA,SCL);
CAN can(CAN_RX,CAN_TX);
SPI spi(A7,A6,A5);
SPI spi5v(B15,B14,B13);


static Ticker ProcessTicker;
volatile bool ProcessTickEvent=false;

void RTSOff(int)    //int is required by callback as an event register
{
  RTS=false;
  return;
}

void ProcessTick(void)
{
    ProcessTickEvent=true;
    return;
}

void Process(void)
{
  uint8_t Data=0x55;
  ExtSPIEn=false;
  spi.write(0x90);
  spi.write(0x0);
  spi.write(0x0);
  spi.write(0x0);
  spi.write(0x0);
  spi.write(0x0);
  spi.write(0x0);
  spi.write(0x0);
  ExtSPIEn=true;
  GreenLED=!(RedLED=!RedLED);
  i2c.abort_transfer();
  i2c.write(0xAA,(const char*)&Data,1,false);
  TTY.printf("%05u,%05u,%05u,%05u,%05u,%05u\r\n",AN0.read_u16(),AN1.read_u16(),AN2.read_u16(),AN3.read_u16(),AN4.read_u16(),AN5.read_u16());
  RTS=true;
  RS485.write((const uint8_t*)&Data,1,callback(RTSOff),SERIAL_EVENT_TX_COMPLETE);
  can.write(CANMessage(0xAA,&Data,1));
  spi5v.write(Data);
  ExtSPIEn=false;
  spi.write(0x7);
  spi.write(0x80);
  ExtSPIEn=true;
  SS=SS+1;
  return;
}

int main() 
{
  Watchdog::get_instance().start(WDT_TIMEOUT);
  i2c.frequency(I2C_BITRATE);
  can.frequency(CAN_BITRATE);
  spi.frequency(100000);
  spi.format(8,3);
  spi5v.frequency(100000);
  ProcessTicker.attach(&ProcessTick,PROCESSING_PERIOD);

  while(1)
  {
        if(ProcessTickEvent)
        {
            ProcessTickEvent=false;
            Process();
        }
  }
}